## Working plan

### Intended starting date
September 2019

### Outline of the goals that you envisage for each of the four year of the duration of the trajectory (150 words).
1. research into the fundamentals of money, economy and videogame history. Review and selection of games that will constitute the corpus. Set up research website and repositories;
2. Writing gameplay logs and analyses. First world building sketches;
3. Gameplay and analyses of corpus finished. Increase number of world building studies;
4. Collect all findings in a dissertation, reviewing artistic and academic results.

### Budget
> There are no scholarships available for the PhDArts programme. There is a limited possibility for reimbursing travelling costs. Although some help can be offered, candidates are responsible for finding accommodation while attending the PhDArts programme in Leiden and The Hague. There are no tuition fees for the PhDArts programme. Please inform us if you have requested any grants.

Due to the strictly digital nature of this research, costs will be mostly concerned with the acquisition of research material (games, books, hardware) and literature. The written output and programming will be done by myself, but it might be necessary to engage with other artists and professional for a project's specific needs (e.g. voice acting, illustration). Though no specific trips are planned at the moment, I would attend relevant conferences and meet up with game creators and specialists in the field, some of which might not be located in the Netherlands.

Outside of my artistic practice I work as a programmer at a design studio in Utrecht, which will be my main source of funding. It is my plan to reduce my workload there to focus on my personal projects, whilst still earning enough money to pay for my living and research expenses.
