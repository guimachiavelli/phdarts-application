## Summary

### Title of research proposal
The effects of gold and credit on building fantastic worlds

### Research question -- Brief formulation of the research question (50 words)

Given the reliance of video games on subcbreated interactive storyworlds, what is the influence of money as both a narrative and gameplay device on the process of world building?

### Proposal summary -- Summary of proposal (150 words and 5 keywords)
In a world that continues to resonate with Fredric Jameson's often quoted passage regarding the impossibility of imagining the end of capitalism, fantasy seems to acquire a vital role in expanding the articulation of the imaginable and the possible. If, as Samuel R. Delany writes, fantasy and science fiction can be understood as largely concerned with the transition from one type of economy to another, the economic system of imaginary worlds in these narratives becomes crucial.

Departing from the perspective that money is an eerie hyperobject that permeates and shapes all worlds -- fictional or otherwise -- the research will investigate the storyworlds found in video games and attempt to understand how money is represented in them, what is its importance to the game's narrative, what functions it has in gameplay, how these functions interact with the story, and, ultimately, how different approaches to money can help shape stories and worlds that provide us with alternatives to our current reality.

_keywords_: money, fantasy, science fiction, videogames, world building

---

